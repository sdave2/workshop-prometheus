<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="Prometheus workshop">
		<meta name="author" content="Eric D. Schabell">

		<title>Lab 2 - Prometheus container</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3 class="r-fit-text">Lab 2 - Installing Prometheus (container image)</h3>
				</section>
				<section>
					<div style="width: 1056px; height: 250px;">
						<h2>Lab Goal</h2>
						<h4>This lab guides you through installing Prometheus using an open source container image on
							your local machine, configuring, and running it to start gathering metrics.</h4>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Container installation notes</h3>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						This lab guides you through installing Prometheus using Podman, an open source container
						tooling set that works on Mac OSX, Linux, and Windows systems. This workshop assumes you have
						installed, initialized, and
						<a href="https://podman.io" target="_blank"><b>running Podman</b></a> on
						your machine:<br />
						<br />
						<ul>
							<li>
								<a href="https://podman.io/docs/installation#installing-on-mac--windows" target="_blank">
								Install on Mac OSX or Windows
								</a>
							</li>
							<li>
								<a href="https://podman.io/docs/installation#installing-on-linux" target="_blank">
									Install on Linux
								</a>
							</li>
						</ul><br />
						<br />
						The rest of this lab is pictured using Mac OSX, but it is assumed you have enough knowledge of
						your Linux or Windows systems to be able to achieve the same results.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Verifying tooling installation</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						To start with a working container installation, you should see the following results for the
						commands shown (note versions shown might differ for you):
					</div>
					<div style="width: 900px; height: 350px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman -v
								podman version 4.5.1

								$ podman machine init
								Downloading VM image: fedora-coreos-38.20230625.2.0-qemu.aarch64.qcow2.xz: done
								Extracting compressed file
								Image resized.
								Machine init complete

								$ podman machine start
								Starting machine "podman-machine-default"
								...
								  (more console output...)
								...
								Machine "podman-machine-default" started successfully

								$ podman machine list
								NAME                     VM TYPE     CREATED     LAST UP            CPUS
								podman-machine-default*  qemu        2 days ago  Currently running  1
							</code>
						</pre>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						Now that you have the tooling, let's get started installing Prometheus in a container...
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Installation - Make a project directory</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						The first step will be to open a console or terminal window and start with the command line
						to create yourself an empty workshop directory, something like this:
					</div>
					<div style="width: 900px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								$ mkdir workshop-prometheus

								$ cd workshop-prometheus
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Setup - A workshop configuration</h3>
					</div>
					<div style="width: 1056px; height: 170px; text-align: left; font-size: xx-large;">
						Using any editor you like, create a file named <code>workshop-prometheus.yml</code> and you are
						going to create a basic Prometheus configuration that looks like the YAML code below which you
						can cut-and-paste (be sure to save the results):
					</div>
					<div style="width: 900px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								# workshop config
								global:
								  scrape_interval: 5s

								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9090"]
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Configuration - Some thoughts on setup</h3>
					</div>
					<div style="width: 1056px; height: 250px; text-align: left; font-size: xx-large;">
						Normally you are monitoring other targets over HTTP and scraping their endpoints, but we are
						going to start with Prometheus as it also exposes its own metrics endpoints. Monitoring your
						Prometheus servers health is only an exercise for this workshop. Also note that scraping metrics
						every 5 seconds is a bit over the top, commonly you would see 10-60 seconds, but we want our
						data to flow in a steady stream for this workshop.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Configuration - The global section</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						As you can imagine, the <code>global</code> section is used for settings and default values.
						Here we have just set the default <code>scrape interval</code> to be 5 seconds:
					</div>
					<div style="width: 900px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								# workshop config
								global:
								  scrape_interval: 5s
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Configuration - The scrape configs section</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						The <code>scrape_configs</code> section is where you tell Prometheus which targets to scrape to
						collect metrics from. In the beginning we will be listing each job for our targets manually,
						using a <code>host:port</code> format:
					</div>
					<div style="width: 900px; height: 170px;">
						<pre>
							<code data-trim data-noescape>
								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9090"]
							</code>
						</pre>

					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						Note: production configurations would use service discovery integrations to find targets, more
						on that later in this workshop.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Installation - Creating container build file</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						Next you can use any editor you like, but create a file called <code>Buildfile</code> with the
						following which you can use through cut-and-pasting:
					</div>
					<div style="width: 900px; height: 250px;">
						<pre>
							<code data-trim data-noescape>
								FROM prom/prometheus:v2.47.0
								ADD workshop-prometheus.yml /etc/prometheus
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h3 class="r-fit-text">Installation - Building a container image</h3>
					</div>
					<div style="width: 1056px; height: 80px; text-align: left; font-size: xx-large;">
						Now you can build your own container image with our custom configuration inserted:
					</div>
					<div style="width: 900px; height: 300px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman build -t workshop-prometheus:v2.47.0 -f Buildfile

								STEP 1/2: FROM prom/prometheus:v2.47.0
								Resolving "prom/prometheus" using unqualified-search
								registries (/etc/containers/registries.conf.d/999-podman-machine.conf)
								Trying to pull docker.io/prom/prometheus:v2.47.0...
								Getting image source signatures
								Copying blob sha256:54512e4fd08c47b3ca9a1a819b67275b22bf3a26be2d068369e330cc2dcaaa30
								Copying blob sha256:ae2aea4a76d0bd645dd670e4146ffb8c6700e5cff84fffc00c57e0546b40e3fe
								Copying blob sha256:9a4e2bd7c8b5eff5563de824d2eda1ab776820b49b8b3371404f796ff14bf5f2
								Copying config sha256:dd202374baaf7882936f1edebed305ff0d1a7dfd9dfb6a09b198f550043d02d2
								Writing manifest to image destination
								Storing signatures
								STEP 2/2: ADD workshop-prometheus.yml /etc/prometheus
								COMMIT workshop-prometheus
								--> a6433a85d68f
								Successfully tagged localhost/workshop-prometheus:v2.47.0
								a6433a85d68faa925da7673a2bbb3bd4a9674f3dca5061d9ae34b5ab1b4d1a77
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Installation - Verifying built image</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Looking at the <code>IMAGES</code> tab in our Podman Desktop we see the images:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman images

								REPOSITORY                     TAG         IMAGE ID      CREATED        SIZE
								localhost/workshop-prometheus  v2.47.0     a6433a85d68f  5 minutes ago  238 MB
								docker.io/prom/prometheus      v2.47.0     dd202374baaf  3 minutes ago  238 MB
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Start your metrics engines!</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						Now it's time to start the Prometheus server. We will point to our config file inside the image
						we built using a flag:
					</div>
					<div style="width: 1000px; height: 30px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman run -p 9090:9090 workshop-prometheus:v2.47.0 --config.file=/etc/prometheus/workshop-prometheus.yml
							</code>
						</pre>
					</div>
					<div style="width: 1056px; height: 350px;">
						<img src="images/lab02-4-podman.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Adjusting settings live</h3>
					</div>
					<div style="width: 1056px; height: 80px; text-align: left; font-size: xx-large;">
						Using this container image we built means any changes you need to make to the configuration
						that were in the flags used to start the server will require a new image be built, the old
						container stopped, and the new container started using the new image.<br />
						<br />
						Now let's see if our Prometheus server is up and running on our local machine by loading the
						status page in our browser at <a href="http://localhost:9090" target="_blank">http://localhost:9090</a>,
						noting it needs to run a little bit to collect some data from its own HTTP metrics endpoint.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - The status page</h3>
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab02-2.png" alt="status"><br />
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						Now let's try the metrics endpoint
						<a href="http://localhost:9090/metrics" target="_blank">(http://localhost:9090/metrics)</a>
						directly in your browser.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 80px;">
						<h3 class="r-fit-text">Prometheus - Live metrics endpoint</h3>
					</div>
					<div style="width: 1056px; height: 550px;">
						<img src="images/lab02-3.png" alt="endpoint"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Checking your targets</h3>
					</div>
					<div style="width: 1056px; height: 150px; text-align: left; font-size: xx-large;">
						After you configure a new <code>prometheus</code> target to scrape and (re)start the
						Prometheus server, validate it's running correctly by going to the status page, using the
						drop down menu at the top labeled <code>STATUS</code> and selecting <code>TARGETS</code>:
					</div>
					<div style="width: 1056px; height: 350px;">
						<img src="images/lab02-4.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Checking your targets</h3>
					</div>
					<div style="width: 1056px; height: 130px; text-align: left; font-size: xx-large;">
						This shows you a list of the targets, in our case just one, featuring the scrape configuration
						details. The most important field here is the <code>STATE</code>, where we want to see a green
						<code>UP</code>:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-5.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Bad target state</h3>
					</div>
					<div style="width: 1056px; height: 170px; text-align: left; font-size: xx-large;">
						Let's break our configuration and see what that looks like in the targets status page. To do this
						open up your configuration file <code>workshop-prometheus.yml</code> and edit the
						<code>scrape_configs</code> section to alter the targets port number as shown:
					</div>
					<div style="width: 900px; height: 170px;">
						<pre>
							<code data-trim data-noescape>
								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9099"]
							</code>
						</pre>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						Save and see next slide for applying the new configuration.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Installation - Building a new container image</h3>
					</div>
					<div style="width: 1056px; height: 80px; text-align: left; font-size: xx-large;">
						Now you can rebuild your own container image with our broken configuration inserted:
					</div>
					<div style="width: 1000px; height: 350px;">
						<pre>
							<code data-trim data-noescape>
								$ podman build -t workshop-prometheus:bad-target -f Buildfile

								STEP 1/2: FROM prom/prometheus:v2.47.0
								STEP 2/2: ADD workshop-prometheus.yml /etc/prometheus
								COMMIT workshop-prometheus
								--> b63d3b6d2139
								Successfully tagged localhost/workshop-prometheus:bad-target
								b63d3b6d2139c3a28eeab4b8d65169a1b4d77da503c51a587340e0a1b0a52b8a
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Installation - Verifying rebuilt image</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						Looking at the <code>IMAGES</code> tab in our Podman Desktop we see the image was rebuilt just
						a bit ago, moving the older image out of the way:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman images

								REPOSITORY                     TAG         IMAGE ID      CREATED             SIZE
								localhost/workshop-prometheus  bad-target  00d6552169cf  About a minute ago  241 MB
								localhost/workshop-prometheus  v2.47.0     d53a1e8ff3dc  7 minutes ago       241 MB
								docker.io/prom/prometheus      v2.47.0     eb8939d5c174  2 hours ago         241 MB
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Start your broken configuration</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: x-large;">
						First you stop the running container, then start it again as we did before, but this time the
						newest rebuilt image will be used:<br />
						<br />
						<code style="font-size: medium;">
							$ podman run -p 9090:9090 workshop-prometheus:bad-target --config.file=/etc/prometheus/workshop-prometheus.yml
						</code>
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-4-podman.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Verify bad target state</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						Back to the target state page and we see that indeed the target we configured is broken:
					</div>
					<div style="width: 1056px; height: 270px;">
						<img src="images/lab02-10.png" alt="broken"><br />
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						<b>Exercise:</b> go back and fix this target before proceeding!
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring command-line flags</h3>
					</div>
					<div style="width: 1056px; height: 170px; text-align: left; font-size: xx-large;">
						When you started Prometheus we mentioned a flag to point at our new configuration file. There is
						a status page you can use to view all the flags that have been set (by your or default settings).
						Using the drop down menu again at the top labeled <code>STATUS</code> and selecting
						<code>COMMAND-LINE FLAGS</code>:
					</div>
					<div style="width: 1056px; height: 350px;">
						<img src="images/lab02-4-flags.png" alt="menu"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring command-line flags</h3>
					</div>
					<div style="width: 1056px; height: 80px; text-align: left; font-size: xx-large;">
						This shows you a long list of flags with their current values and a search field for locating
						one you might be interested in:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-6.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Searching for a flag</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						By filling in a search query, you can narrow down the long list to a specific area. Let's explore
						the flag value for <code>--config.file</code>:
					</div>
					<div style="width: 1056px; height: 300px;">
						<img src="images/lab02-7.png" alt="config"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Time series database status</h3>
					</div>
					<div style="width: 1056px; height: 130px; text-align: left; font-size: xx-large;">
						Next, there is a status page you can use to view your time series database, or TSDB, status.
						Using the drop down menu again at the top labeled <code>STATUS</code> and selecting
						<code>TSDB-STATUS</code>:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-4-status.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Time series database status</h3>
					</div>
					<div style="width: 1056px; height: 80px; text-align: left; font-size: xx-large;">
						This shows you some details for the time series being collected in an overview status line with
						several tables below with cardinality status:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-8.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring active configuration</h3>
					</div>
					<div style="width: 1056px; height: 130px; text-align: left; font-size: xx-large;">
						Lastly, at least in this lab, you can verify the exact configuration being applied to your
						Prometheus server. Using the drop down menu again at the top labeled <code>STATUS</code> and selecting
						<code>CONFIGURATION</code>:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-4-config.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring active configuration</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						This shows you your exact configuration, often including some defaults that you might not have
						in your personal configuration file, yet are begin used. There is even a handy copy-to-clipboard
						button for you to grab it:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-9.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Using the expression tooling</h3>
					</div>
					<div style="width: 1056px; height: 170px; text-align: left; font-size: xx-large;">
						You can navigate back to the expression browser that let's you query your time series data
						by clicking on the menu entry <code>GRAPH</code>. This is the default built-in query interface
						for running Prometheus Query Language (PromQL) queries. Be sure you are in the <code>TABLE</code>
						tab:
					</div>
					<div style="width: 1056px; height: 300px;">
						<img src="images/lab02-11.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Total samples ingested</h3>
					</div>
					<div style="width: 1056px; height: 250px; text-align: left; font-size: xx-large;">
						The <code>TABLE</code> view provides the output of a query written using PromQL expression as
						a series. It's less expensive to use than the other option, <code>GRAPH</code>, because you are
						not plotting out the series answers in a graph. Without worrying about the PromQL used (we'll
						explore that later in this workshop), lets show the total number of samples ingested by our
						Prometheus server since it started:
					</div>
					<div style="width: 1000px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								prometheus_tsdb_head_samples_appended_total
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Validating an expression</h3>
					</div>
					<div style="width: 1056px; height: 200px; text-align: left; font-size: xx-large;">
						You will have noticed that there are three buttons to the right of the <code>EXPRESSION</code>
						entry field. The first one can be used to validate the expression you entered, just click and if
						it's a good expression you get a checkmark. Enter the previous slides expression and click on
						the first button to generate a checkmark:
					</div>
					<div style="width: 1056px; height: 200px;">
						<img src="images/lab02-12.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the expression query</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						After validating our expression, run it by clicking on the <code>EXECUTE</code> button:
					</div>
					<div style="width: 1056px; height: 300px;">
						<img src="images/lab02-13.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring the visualization</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						Let's explore by clicking on the <code>GRAPH</code> tab. Note the features included to
						visualize queries. I have adjusted my longer running server to look at 12h of data here and
						am browsing the results:
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab02-13a.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Add another panel</h3>
					</div>
					<div style="width: 1056px; height: 170px; text-align: left; font-size: xx-large;">
						Go back to the <code>TABLE</code> tab. Notice at the bottom there is a button <code>ADD PANEL</code>,
						click on it to add another query panel in which we will execute the following query expression.
						Let's look at the number of samples ingested per second averaged over a 1m window of time:
					</div>
					<div style="width: 1000px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								rate(prometheus_tsdb_head_samples_appended_total[1m])
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the second query</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						You can now see multiple expressions are possible:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-14.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 80px;">
						<h3 class="r-fit-text">Prometheus - Exploring second visualization</h3>
					</div>
					<div style="width: 1056px; height: 550px;">
						<img src="images/lab02-15.png" alt="graphs"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Intermezzo - Warning about pages</h3>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						If you now select any of the status pages from the <code>STATUS</code> menu at the top and
						then return to the expression query page using the <code>GRAPH</code> menu entry, you will
						notice that extra panels you might have added will be gone.<br />
						<br />
						<b>Pro tip:</b> you might want to work using browser tabs.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - One last query</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						Assuming you went to another status page and back, we have a last query we will run here to
						simulate the same query used to fill the system UP metric we viewed for our Prometheus target:
					</div>
					<div style="width: 1000px; height: 250px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								up{job="prometheus"}
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the second query</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						You can now see the results as a boolean value suggesting it is really UP:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab02-16.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring this silly visualization</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						This is a rather silly metric to visualize this way, but this is what it looks like going back
						over 1d as my server was running during this lab's development (you can check my working hours!):
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab02-17.png" alt="graphs"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 150px;">
						<h2 class="r-fit-text">Lab completed - Results</h2>
					</div>
					<div style="width: 1056px; height: 400px; font-size: xx-large">
						<img src="images/lab02-2.png" alt="completed">
						<br />
						Next up, exploring the query language...
					</div>
				</section>

				<section data-background="images/questions.png">
					<span class="menu-title" style="display: none">References</span>
					<div style="width: 1056px; height: 200px;">
						<img height="150" width="100%" src="images/references.jpg" alt="references">
					</div>
					<div style="width: 1056px; height: 400px; font-size: xx-large; text-align: left">
						<ul>
							<li><a href="https://youtu.be/KJvUyP4c1NQ" target="_blank">Video introduction to workshop (Open Source Edinburgh meetup)</a></li>
							<li><a href="https://o11y-workshops.gitlab.io/" target="_blank">Getting started with cloud native o11y workshops</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus" target="_blank">This workshop project repository</a></li>
							<li><a href="https://gitlab.com/o11y-workshops" target="_blank">O11y workshop collection</a></li>
							<li><a href="https://prometheus.io" target="_blank">Prometheus project</a></li>
							<li><a href="https://github.com/prometheus/promlens" target="_blank">PromLens project</a></li>
							<li><a href="https://github.com/perses/perses" target="_blank">Perses project</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus/-/issues/new" target="_blank">Report an issue with this workshop</a></li>
						</ul>
					</div>
				</section>

				<section>
					<span class="menu-title" style="display: none">Questions or feedback?</span>
					<div style="width: 1056px; height: 150px">
						<h2 class="r-fit-text">Contact - are there any questions?</h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: x-large; text-align: left">
						Eric D. Schabell<br/>
						Director Evangelism<br/>
						Contact: <a href="https://twitter.com/ericschabell" target="_blank">@ericschabell</a>
						{<a href="https://fosstodon.org/@ericschabell" target="_blank">@fosstodon.org</a>)
						or <a href="https://www.schabell.org" target="_blank">https://www.schabell.org</a>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 250px;">
						<h2 class="r-fit-text">Up next in workshop... </h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: xxx-large;">
						<a href="lab03.html" target="_blank">Lab 3 - Introduction to the Query Language</a>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMenu, RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
	</body>
</html>